//Author: Derin Lucio
#ifndef _ALLOCATION
#define _ALLOCATION
#include <iostream>

//OUTPUT MODE:
//0 = SILENT
//-1 = ONLY MEMORY USAGE
//1 = VERBOSE
static int s_outputMode = 1;

//Struct to store the data of the memory usage
struct MemoryUsage{
    //total allocated memory and total freed memory
    uint64_t allocatedMemory, freedMemory;
    //current size of occupied memory
    uint64_t CurrentUsage() { return allocatedMemory - freedMemory; }
};
//Static instantiation of the struct
static MemoryUsage s_memoryUsage;

//overloading of void* operator new(size_t)
void* operator new(size_t size){
    //storing the allocated address
    void* alloc = malloc(size);
    //updating total allocated memory
    s_memoryUsage.allocatedMemory += size;
    //Verbose output
    if(s_outputMode==1){
        //print of the updated memory usage
        std::cout << "* Allocated " << size << " bytes on the heap in address " << alloc << ";\n";
        std::cout << "  - Current memory usage: " << s_memoryUsage.CurrentUsage() << " bytes;\n";
    }
    //only memory usage output
    else if(s_outputMode == -1){
        std::cout << "  - Current memory usage: " << s_memoryUsage.CurrentUsage() << " bytes;\n";
    }

    //returning the void* address, as the default "new" operator does
    return alloc;
}

//overloading of void operator delete(void*,size_t)
void operator delete(void* memory, size_t size){
    //updating total freed memory
    s_memoryUsage.freedMemory += size;
    //freeing the memory at address void* memory, as the default "delete" operator does
    free(memory);

    //Verbose output
    if(s_outputMode==1){
        //print of the updated memory usage
        std::cout << "* Deallocating " << size << "bytes on the heap in address: " << memory << ";\n";
        std::cout << "  - Current memory usage: " << s_memoryUsage.CurrentUsage() << " bytes;\n";
    }
    //only memory usage output
    if(s_outputMode==-1){
        std::cout << "  - Current memory usage: " << s_memoryUsage.CurrentUsage() << " bytes;\n";
    }
}
#endif
