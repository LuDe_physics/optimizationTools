//Author: Derin Lucio
#include <iostream>
#include <vector>
#include <memory>
#include "allocation.h"
#include "timer.h"

//testing struct
struct Obj{
    int a;
};

//declaring an Obj in a smaller scope
void allocate(){
    //Timer
    //This object will print its lifetime, i.e. the allocate() execution time.
    //note that Timer uses strings, which are dynamically allocated and will affect the memory usage counter;
    //comment this declaration to see the memory usage of the objects in this function only.
    Timer t1("- allocate() duration: ");

    //this obj is declared as a smart pointer
    //it is allocated on the heap; the smart pointer will automatically call "delete" operator as the scope ends
    std::unique_ptr<Obj> obj = std::make_unique<Obj>();
}

int main(){
    //Timer
    //This object will print its lifetime, i.e. the main() execution time.
    //note that Timer uses strings, which are dynamically allocated and will affect the memory usage counter;
    //comment this declaration to see the memory usage of the object in this main() only
    Timer t("- main() duration: ");

    //std::vector are allocated on the heap: this vector will occupy 16 bytes of memory (2 doubles)
    //note that this does not call the "delete" operator
    std::vector<double> x = {2.,1.};

    //this obj is allocated on the stack, so it will not trigger "new" or "delete" operator
    Obj a;
    allocate();
}