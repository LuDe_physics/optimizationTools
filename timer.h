//Author: Derin Lucio
//C++ class that implements a timer for benchmarking
//An object of this class, if instantiated, will print its own lifetime
//i.e. it will print the time elapsed between its declaration and the end of the scope in which it's declared
#ifndef _TIMER
#define _TIMER
#include <string>
#include <chrono>
#include <iostream>

class Timer
{
private:
    //pointer to the start time
    std::chrono::time_point<std::chrono::high_resolution_clock> m_startTimeptr;
    //string to comment the output
    std::string m_output;

public:
    //constructor
    //starts the timer
    Timer(std::string output = "Measured time: ") : m_output(output) { m_startTimeptr = std::chrono::high_resolution_clock::now(); }
    //destructor
    //stops the timer and prints the result in milliseconds with microsecond resolution
    ~Timer()
    {
        auto endTimeptr = std::chrono::high_resolution_clock::now();
        auto startTime = std::chrono::time_point_cast<std::chrono::microseconds>(m_startTimeptr).time_since_epoch().count();
        auto endTime = std::chrono::time_point_cast<std::chrono::microseconds>(endTimeptr).time_since_epoch().count();
        auto deltaT = (endTime - startTime);
        double deltaTms = (double)deltaT * 0.001;
        std::cout << m_output << deltaTms << "ms;\n";
    }
};
#endif
