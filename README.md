# Set of c++ tools for benchmarking and optimization

## Timer for benchmarking:
### Usage
- Include "timer.h" header in the source;
- Instantiate an object of the timer class:
    - Constructor:
        ```
        void Timer(std::string output = "Measured time: ");
        ```
        "output" is the message that will be printed before the measured time
- The constructor starts the timer and the destructor stops it: this way, the instantiated object will measure its own lifetime (i.e. the time elapsed between its declaration and the end of the scope in which it is declared);

## Overloaded operator "new" and "delete":
### Usage
- Include "allocation.h" header in the source;
- Every time they are called, the overloaded "new" and "delete" operators, depending on output mode, will print:
    - Output mode: verbose (int s_outputMode = 1, defined in allocation.h):
        - respectively, the allocated and freed heap memory;
        - the affected address of heap memory;
        - the current heap memory usage of the program;
    - Output mode: only current usage (int s_outputMode = -1, defined in allocation.h):
        - the current heap memory usage of the program;
    - Output mode: silent (int s_outputMode = 0, defined in allocation.h):
        - no output;
